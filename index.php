<?php
require __DIR__ .'/vendor/autoload.php';

use MyNamespace\Developer;
use MyNamespace\Settings;

//header('Location: http://www.example.com/');
//header('Location: http://folder.172.16.10.221.nip.io');

$dbConnectionSettings = [
    'MYSQL_HOST'        => 'db',
    'MYSQL_DATABASE'    => 'db',
    'MYSQL_USER'        => 'db',
    'MYSQL_PASSWORD'    => 'db'
];

$newDbConnectionSettings = [
    'MYSQL_HOST'        => 'newDb',
    'MYSQL_DATABASE'    => 'newDb',
    'MYSQL_USER'        => 'newDb',
    'MYSQL_PASSWORD'    => 'newDb'
];

$test = new Developer('Maksssssssssim','backend');

try {
    if (strlen($test->name) > 10) {
        throw new Exception('Name is long');
    }
} catch (Exception $e) {
    echo $e->getMessage();
}

$test2 = new Developer('Dima', 'frontend');

echo '<pre>';
var_dump($test);
echo '</pre>';

$test2->UpPosition();

echo '<pre>';
var_dump($test2);
echo '</pre>';

echo " поток вывода: имя объекта test до буфера ";
ob_start();
echo "$test->name Kipa";
$content = ob_get_contents();
ob_flush();
echo '1';
$content2 = ob_get_contents();
echo "</br> значение буфера - $content";
echo "</br> новое значение буфера - ";
echo  $content2;
echo "</br>первый метод интерфейса: ";
echo $test2->eat("бургер") . " " . $test2->name;
echo "</br>первый метод интерфейса: ";
echo "A " . $test->drink("колу") . " " . $test->name;
echo "</br>" . 'метод с параметром объектом (return $this->name): ';
echo $test->work($test);
echo "</br> трейт ";
echo $test->DrinkCoffee();
echo "</br> </br> Singleton";
$singleObject['First'] = Settings::getInstance();
$singleObject['First']->setVar($dbConnectionSettings);
$singleObject['Second'] = Settings::getInstance();
$singleObject['Second']->setVar($newDbConnectionSettings);
echo '<pre>';
var_dump($singleObject);
echo '</pre>';
//ob_start();
//все команды вывода далее в буфере
//$content = ob_get_contents(); // сохранение буфера
//header("PHP-VERSION: ". PHP_VERSION);
//Размер загружаемого файла настраивается в файле php. ini строчкой upload_max_filesize = 10M
 //Settings::get()->items_per_page = 20;
//echo Settings::get()->items_per_page;

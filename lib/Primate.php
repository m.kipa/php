<?php

namespace MyNamespace;

interface Primate
{
    public function eat(string $productEatName);
    public function drink(string $productDrinkName);
}
<?php

namespace MyNamespace;

final class Settings
{
    private static $instance = null;
    private $var;
    
    /**
     * запрещаем прямое создание
     */
    private function __construct()
    {
        //
    }

    /**
     * запрещаем клоинирование
     */
    private function __clone()
    {
        //
    }
    
    /**
     * запрещаем десериализацию
     */
    private function __wakeup()
    {
        //
    }

    public static function getInstance()
    {
        /**
         * важный метод
         */
        if (is_null(self::$instance)) {
            self::$instance = new Settings();
        }
        return self::$instance;
    }

    public function setVar($value)
    {
        $this->var = $value;
    }
}


<?php

namespace MyNamespace;

class Developer extends Human implements Primate
{
    use traits\OfficePlankton;
    
    public string $name;
    public string $post;
    public string $position;
    
    /**
     * @param string $name
     * @param string $post
     */
    public function __construct(string $name, string $post)
    {
        $this->name     = $name;
        $this->post     = $post;
        $this->position = "Junior";
    }
    
    /**
     * @param string $productEatName
     * @return string
     */
    public function eat(string $productEatName): string
    {
        return "Я ем $productEatName";
    }
    
    /**
     * @param string $productDrinkName
     * @return string
     */
    public function drink(string $productDrinkName): string
    {
        return "Я пью $productDrinkName";
    }
    
    /**
     * @return string
     */
    public function upPosition(): string
    {
        return $this->position = "Middle";
    }
    
    /**
     * метод получающий на вход объект
     * @param object $object
     * @return string
     */
    public function work(object $object): string
    {
        return $this->name . " работает";
    }
}

<?php

namespace MyNamespace;

abstract class Human
{
    public function walkToWork(): string
    {
        return "Ходить на работу";
    }
}

<?php
if ($_FILES['filename']['size'] > 3 * 1024 * 1024) {
    exit();
    echo 'Размер файла превышает три мегабайта';
}
if (move_uploaded_file(
    $_FILES['filename']['tmp_name'],
    'temp/' . $_FILES['filename']['name']))
{
    echo 'Файл успешно загружен <br />';
} else {
    echo 'Ошибка загрузки файла <br />';
}

/*
 * Макснмальный размер загружаемого файла можно также задать при помощи
 *  директивы upload_max_filesize, значение которой по умолчанию равно 2 Мбайт:
 * if ($_FILES['filename'] ['size'] > upload_max_filesize)
*/
